/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  tau@qerana.com
 * Created: Mar 25, 2021
 */


CREATE TABLE `qerana_fw`.`example` (
  `id_example` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `age` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_example`));

--  dump data --
INSERT INTO `qerana_fw`.`example` (`name`, `age`) VALUES ('kiss', '8');
INSERT INTO `qerana_fw`.`example` (`name`, `age`) VALUES ('solid', '4');
INSERT INTO `qerana_fw`.`example` (`name`, `age`) VALUES ('dry', '2');
