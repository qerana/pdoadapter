<?php

/*
 * Copyright (C) 2021 qerana@qerana.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Archivo de ejemplo sobre CRUD con \Qerana\Adapter\PdoAdapter.php
 * 
 * Copiar la carpeta en la raiz de un vhost,
 * Cambiar el archivo db_config.json, con la conexion a su DB.
 * 
 */

namespace MySuperApp;

use Qerana\Adapter\PdoAdapter;
use Qerana\Adapter\connection\ConfigHelper,
    Qerana\Adapter\connection\SqlPDO;

// autoload PSR-4,
require 'vendor/autoload.php';

// archivo de configuracion json, cambiar por la ruta correspondiente
$json_db = __DIR__ . 'db_config.json';

try {

    // parseamos el archivo de configuracion
    $DbSettings = ConfigHelper::getDbSettings($json_db);

// enviamos la configuracion para base de datos uno, definido en el json
    $SingletonDb1 = SqlPDO::singleton($DbSetting->database1);

    // creamos PdoAdapter, inyectando la base de datos 1, con la tabla example
    $PdoAdapterDb1 = new PdoAdapter($SingletonDb1,'example');
    
} catch (\Exception $ex) {
    echo $ex->getMessage();
    exit();
}

/**
 * Finders, 
 * 
 * Metodos de consultas rapidas
 * 
 * @
 * 
 */

// todos los registros
$datas = $PdoAdapterDb1->find();
echo '<pre>';
print_r($data);
die();
// un registro id_example = 4

$data = $PdoAdapterDb1->findOne(['id_example' => 4]);

print_r($data);
// registros ordenados por edades ASC
$datas_edades = $PdoAdapterDb1->find('',['orderby'=>'age ASC']);

// traemos todos los registros





