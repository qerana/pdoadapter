<?php

/*
 * Copyright (C) 2021 qerana@qerana.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\Adapter\connection;

use PDO;

class SqlPDO extends PDO
{

    private static $instance = null;

    public function __construct($Config)
    {


        // array de opciones para pdo
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // error exception mode
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'" // utf8 always
        ];

        try {
            parent::__construct('mysql:host=' . $Config->hostname .
                    ';port=' . $Config->port . ';dbname=' .
                    $Config->name, $Config->username, $Config->password, $options);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * singleton 
     * @param type $Conn
     * @return type
     */
    public static function singleton($Conn)
    {
        if (self::$instance == null) {

            self::$instance = new self($Conn);
        }

        return self::$instance;
    }

}
