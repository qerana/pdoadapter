<?php

/*
 * Copyright (C) 2021 qerana@qerana.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\Adapter\connection;

/**
 * Lee un un archivo de configuracion en json 
 * 
 * @example json de configuracion de conexiones a bases de datos
 * 
 * {
  "database1": {
  "hostname": "localhost",
  "port": "3309",
  "name": "db1",
  "username": "user1",
  "password": "pass1"
  },
  "database2": {
  "hostname": "1.1.1.1",
  "port": "3306",
  "name": "db2",
  "username": "user2",
  "password": "pass2"
  }
  }
 * $settings_json_file = __DIR__.' config/db_config.json';
 * $DbSettings = \Qerana\Adapter\connection\ConfigHelper::getDbSettings($setting_json_file);
 * 
 * Para conectar a la base de datos 1 (database1)
 * $Conn = \Qerana\Adapter\connection\SqlPDO($DbSetting->database1);
 *
 * Para conectar a la base de datos 2 (database2)
 * $Conn = \Qerana\Adapter\connection\SqlPDO($DbSetting->database2);
 *
 * 
 * 
 * @author tau@qerana.com
 */
class ConfigHelper
{

    
    /**
     * lee un archivo de configuracion json, lo transforma en un objeto 
     * @param string $path , ruta completa del archivo json
     * @return \stdClass
     * @throws \InvalidArgumentException
     */
    public static function getDbSettings(string $path): \stdClass
    {

        $real_path = realpath($path);

        if (!$real_path) {
            throw new \InvalidArgumentException('El archivo de configuracion a la DB :' . $path . ', no existe.');
        } else {
            $json_config = file_get_contents($real_path);
            return json_decode($json_config);
        }
    }

}
