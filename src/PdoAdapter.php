<?php

namespace Qerana\Adapter;

use PDO;
use PDOException;
use RuntimeException;


/**
 *  PdoAdapter
 *
 * Adaptador PDO, la idea es encapsular las querys, y facilitar mediante finders
 * consultas simples
 *
 * @author tau@qerana.com
 */
class PdoAdapter
{

    private
        /** @object, db connection instance  */
        $DbConnection;
        /** @string PDO STATEMENT */
        private $Statement;
        /** @string mode to fetch PDO */
        private $fetch_mode = PDO::FETCH_ASSOC;
        /**  @string  name off table */
        private $table_name;
        /**  @string  query for finders */
        private $query_find;
        /**  @array binds for query finder */
        private $find_binders = [];

    /**
     *
     * @param PDO $DB
     * @param string|null $table_name
     */
    public function __construct(PDO $DB, string $table_name = null)
    {
        $this->DbConnection = $DB;

        if (!is_null($table_name)) {
            $this->setResource($table_name);
        }
    }


    public function setFetchMode(string $fetch){
        $this->fetch_mode = $fetch;
    }

    /**
     * Devuelve la query
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query_find;
    }

    /**
     * Finder, ejecuta consultas mediante unas condiciones
     * @param mixed $conditions
     * @param array $options
     * @param  string $fields
     * @return type
     */
    public function find($conditions = [], array $options = [], $fields = '*')
    {

        $this->checkTableName();

        $this->query_find = ' SELECT ' . $fields . ' FROM ' . $this->table_name;

        if (is_array($conditions)) {

            // build conditions array based
            $this->parseConditions($conditions, '=');
        } else {

            // pàrsea el raw query para agregar binds
            $this->parseRawConditions($conditions, $options);

            $this->query_find .= ' WHERE ' . $conditions;
        }

        $this->query_find .= (key_exists('groupby', $options)) ? ' GROUP BY ' . $options['groupby'] : '';
        $this->query_find .= (key_exists('orderby', $options)) ? ' ORDER BY ' . $options['orderby'] : '';
        $this->query_find .= (key_exists('limit', $options)) ? ' LIMIT ' . $options['limit'] : '';


        $this->prepare($this->query_find)->execute($this->find_binders);
        $mode = (isset($options['fetch'])) ? $options['fetch'] : 'all';
        return ($mode == 'all') ? $this->fetchAll() : $this->fetch();
    }

    /**
     * Ejecuta find all, pero devuelve oslo un resultado
     * @param type $conditions
     * @param array $options
     * @param type $fields
     * @return type
     */
    public function findOne($conditions = [], array $options = [], $fields = '*')
    {

        $options['fetch'] = 'one';
        $options['limit'] = 1;
        return $this->find($conditions, $options, $fields);
    }

    /**
     * Devuelve resultados de una raw query
     * @param string $query
     * @param array $binds
     * @param string $mode
     * @return array
     */
    public function findByQuery(string $query, array $binds = [], string $mode = 'all')
    {
        $this->query_find = $query;
        $this->prepare($this->query_find)->execute($binds);
        $retval = ($mode == 'all') ? $this->fetchAll() : $this->fetch();

        if (empty($retval)) {
            return [];
        } else {
            return $retval;
        }
    }

    /**
     * Parsea la query para ofrecer soporte de binds parametros
     * @param string $condition
     * @param array $options
     * @throws RuntimeException
     */
    private function parseRawConditions(string $condition, array $options)
    {
        // buscamos todos los bind para poder crear binds en cada uno
        preg_match_all('/:b/i', $condition, $matches);


        // si no es vacio entonces, buscamos en options, si se ha enviado el array de binds
        if (!empty($matches)) {

            if (isset($options['binds']) AND (!empty($options['binds']))) {

                $binds = $options['binds'];

                $i = 0;
                foreach ($binds AS $key => $value):
                    $i++;
                    $bind_val = ':b' . $i;
                    $this->find_binders[$bind_val] = $value;

                endforeach;
            } else {
                throw new \Exception('En Raw finders, hay que enviar binds, como segundo parametro $options');
            }
        }
    }

    /**
     * Realiza una insercion en la base de datos
     * @param array $data
     * @param type $table
     * @return int
     */
    public function insert(array $data, $table = ''): int
    {

        $table_name = (empty($table)) ? $this->table_name : $table;


        $cols = implode(', ', array_keys($data));
        $values = implode(', :', array_keys($data));

        // creamos  los data binds
        foreach ($data as $col => $value) {
            unset($data[$col]);
            $data[":" . $col] = $value;
        }

        $query_insert = 'INSERT INTO ' . $table_name . ' (' . $cols . ') VALUES (:' . $values . ')';

        return $this->prepare($query_insert)->execute($data)->getLastInsertId();
    }

    /**
     * Actualiza un registro de la tabla
     * @param array $data
     * @param array $conditions
     * @param type $table
     * @return type
     */
    public function update(array $data, array $conditions = [], $table = '')
    {

        $table_name = (empty($table)) ? $this->table_name : $table;

        $binds = [];

        // data binds
        foreach ($data AS $col => $value):

            unset($data[$col]);
            $binds[':' . $col] = $value;
            $data[] = $col . '= :' . $col;
        endforeach;

        $query_update = 'UPDATE ' . $table_name . ' SET ' . implode(', ', $data);

        // build conditions array based
        if ($conditions):
            $c = 0;
            foreach ($conditions AS $field => $search) :
                $c++;
                $boolean_operator = ($c == 1) ? ' WHERE ' : ' AND ';
                $query_update .= $boolean_operator . $field . ' = :' . $field . '';
                $binds[':' . $field] = $search;
            endforeach;
        endif;
        return $this->prepare($query_update)->execute($binds);
    }

    /**
     *
     * @param array $conditions
     * @param type $table
     * @return type
     */
    public function delete(array $conditions, $table = '')
    {
        $table_name = (empty($table)) ? $this->table_name : $table;
        $query_delete = ' DELETE FROM ' . $table_name . ' ';

        // build conditions array based
        $c = 0;
        foreach ($conditions AS $col => $value) :
            $c++;
            unset($conditions[$col]);
            $boolean_operator = ($c == 1) ? ' WHERE ' : ' AND ';
            $query_delete .= $boolean_operator . $col . ' = :' . $col . '';
            $conditions[':' . $col] = $value;
        endforeach;

        return $this->prepare($query_delete)->execute($conditions);
    }

    /**
     * Setea el recurso, es decir el nombre de la tabla
     * @param string $table_name
     * @return void
     */
    public function setResource(string $table_name): void
    {
        $this->table_name = $table_name;
    }

    /**
     * Obtiene el statement
     * @return object
     * @throws PDOException
     */
    private function getStatement(): object
    {
        if ($this->Statement === null) {
            throw new PDOException('No PDO Statetment object');
        }

        return $this->Statement;
    }

    /**
     * Prepara la query
     * @param string $sql
     * @param array $options
     * @return object
     * @throws RuntimeException
     */
    private function prepare(string $sql, array $options = []): object
    {
        try {
            $this->Statement = $this->DbConnection->prepare($sql, $options);
            return $this;
        } catch (PDOException $ex) {
            throw new \Exception($ex->getMessage() . '.SQL:' . $sql);
        }
    }

    /**
     * Checkea si esta seteada el nombre de la tabla
     */
    protected function checkTableName(): void
    {
        if (empty($this->table_name)) {
            throw new \Exception(sprintf('No ha sido seteada ninguna tabla: %s',$this->table_name));
        }
    }

    /**
     * Ecuta el statements
     * @param array $binds
     * @return $this
     * @throws RuntimeException
     */
    private function execute(array $binds = [])
    {

        try {
            $this->getStatement()->execute($binds);
            return $this;
        } catch (PDOException $ex) {
            throw new \Exception($ex->getMessage().'-'.print_r($binds,true));
        }
    }

    /**
     * Devuelve el resultado segun el modo seleccionado
     * @param type $fetch_mode
     * @param type $orientation
     * @param type $offset
     * @return type
     * @throws RuntimeException
     */
    private function fetch($fetch_mode = null, $orientation = null, $offset = null)
    {
        $fetch_style = (is_null($fetch_mode)) ? $this->fetch_mode : $fetch_mode;

        try {
            return $this->getStatement()->fetch($fetch_style, $orientation, $offset);
        } catch (PDOException $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * Devuelve todo
     * @param type $fetch_mode
     * @param type $column
     * @return array
     * @throws RuntimeException
     */
    private function fetchAll($fetch_mode = null, $column = 0): array
    {
        $fetch_style = $fetch_style = (is_null($fetch_mode)) ? $this->fetch_mode : $fetch_mode;

        try {

            return ($fetch_style === PDO::FETCH_COLUMN) ?
                $this->getStatement()->fetchAll($fetch_style, $column) :
                $this->getStatement()->fetchAll($fetch_style);
        } catch (PDOException $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * Parsea las conditions, a usar en la query, bindea todos los valores
     * @param array $conditions
     */
    private function parseConditions(array $conditions = [])
    {
        $c = 0;
        foreach ($conditions AS $field => $search) :
            $c++;
            $boolean_operator = ($c == 1) ? ' WHERE ' : ' AND ';

            $this->buildQueryAndBinds($field, $search,
                [
                    'boolean_operator' => $boolean_operator,
                    'operator' => '='
                ]);


        endforeach;
    }

    /**
     * Bindea cada elemento de una condicion
     * @param type $field
     * @param type $value
     * @param array $options
     */
    private function buildQueryAndBinds($field, $value, array $options = [])
    {
        $array_field = explode('.', $field);
        $field_without_alias = end($array_field);


        $this->query_find .= $options['boolean_operator'] . $field . ' ' . $options['operator'] . ' :' . $field_without_alias . '';

        // vacimos los binders seteados anteriorment

        $this->find_binders[':' . $field_without_alias] = $value;
    }

    /**
     * Devuelve el ultimo id
     * @param type $name
     * @return type
     */
    private function getLastInsertId($name = null)
    {
        try {
            return $this->DbConnection->lastInsertId($name);
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function runQuery($query, array $binds)
    {
        return $this->prepare($query)->execute($binds);

    }

}
